Installation
------------
Install ROS Melodic
Install Gazebo9
Install Repo at https://gitlab.com/HCRLab/stingray-robotics/Stingray-Simulation

Place this ROS Package "lidar_nav_triton_zudans" in your catkin_ws directory

Run From the catkin_ws
----------------------
catkin_make 
source /opt/ros/melodic/setup.bash
source ~/catkin_ws/devel/setup.bash #will change based upon where your catkin_ws is
source ~/stingray_setup.bash #this will depend on your instalation of the StingrayRepo
roslaunch lidar_nav_triton_zudans wall_following_P2D1_Zudans.launch

Notes on Roslaunch
------------------
The above roslaunch command will launch the triton-maze-large.world in gazebo, with a stingray_sim node, and lidar_nav_triton_zudans node. Please make sure you have the gazebo resources sourced properly through the sting

