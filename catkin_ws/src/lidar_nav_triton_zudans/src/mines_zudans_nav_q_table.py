#!/usr/bin/env python
import rospy
#from geometry_msgs.msg import Twist
#from turtlesim.msg import Pose
#from sensor_msgs/LaserScan.msg import LaserScan\
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Pose2D
import math
import time
import numpy as np
from enum import Enum



class Distance(Enum):
     TOO_CLOSE = 0
     CLOSE = 1
     MEDIUM = 2
     TOO_FAR = 3

#global vars
curr_state = (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.TOO_CLOSE)#left, forward, and right distances
curr_action = 0#curent turn action -1=turn left, 0 = none, 1 = right
follow_right = True
has_seen_wall = False
pub = rospy.Publisher('triton_lidar/vel_cmd', Pose2D, queue_size=2)

#constants
rosHZ = 1
turnSpeed = 3
linearSpeed = 0.5

#

#q-table-manual: dictionary that takes in a 3-tuple of distances and outputs a tuple of action values
#used for right wall following
qTableManual =	{
#Follow Straight Wall at good distance
  (Distance.TOO_FAR,Distance.TOO_FAR,Distance.TOO_CLOSE): (1,0,0),
  (Distance.TOO_FAR,Distance.TOO_FAR,Distance.CLOSE): (0,1,0),
  (Distance.TOO_FAR,Distance.TOO_FAR,Distance.TOO_FAR): (0,0,1),
  (Distance.CLOSE,Distance.TOO_FAR,Distance.TOO_CLOSE): (1,0,0),
  (Distance.CLOSE,Distance.TOO_FAR,Distance.CLOSE): (0,1,0),
  (Distance.CLOSE,Distance.TOO_FAR,Distance.TOO_FAR): (0,0,1),
  (Distance.TOO_CLOSE,Distance.TOO_FAR,Distance.TOO_CLOSE): (1,0,0),
  (Distance.TOO_CLOSE,Distance.TOO_FAR,Distance.CLOSE): (0,1,0),
  (Distance.TOO_CLOSE,Distance.TOO_FAR,Distance.TOO_FAR): (0,0,1),

#left if theres an obstacle in from of you
  (Distance.TOO_FAR,Distance.CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.TOO_FAR,Distance.CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.TOO_FAR,Distance.CLOSE,Distance.TOO_CLOSE): (1,0,0),
  (Distance.TOO_FAR,Distance.TOO_CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.TOO_FAR,Distance.TOO_CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.TOO_FAR,Distance.TOO_CLOSE,Distance.TOO_CLOSE): (1,0,0),

  (Distance.CLOSE,Distance.CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.CLOSE,Distance.CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.CLOSE,Distance.CLOSE,Distance.TOO_CLOSE): (1,0,0),
  (Distance.CLOSE,Distance.TOO_CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.CLOSE,Distance.TOO_CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.CLOSE,Distance.TOO_CLOSE,Distance.TOO_CLOSE): (1,0,0),

  (Distance.TOO_CLOSE,Distance.CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.TOO_CLOSE,Distance.CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.TOO_CLOSE,Distance.CLOSE,Distance.TOO_CLOSE): (1,0,0),
  (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.TOO_CLOSE): (1,0,0),


}


# def move():
#     pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=10)
#     directions = Twist()
#     directions.linear.x = 0.0
#     directions.angular.z= 2
#     loopRate = rospy.Rate(rosHZ)
#
#     while True :
#         pub.publish(directions)
#         loopRate.sleep()
#
# def move_m():
#     pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=10)
#     directions = Twist()
#     #directions.linear.x = 0.0
#     #directions.angular.z= 2
#     loopRate = rospy.Rate(rosHZ)
#     time.sleep(2)
#     for i in range(len(t_instructions)):#for each machine instruction
#         pub.publish(t_instructions[i]) #publish machine instruction
#         loopRate.sleep()
#     pub.publish(Twist())#stop
    

def getDistance(distance):
    if(distance<0):
        return "Error negative distance"
    elif(distance<0.72):
        return Distance.TOO_CLOSE
    elif(distance<0.78):
        return Distance.CLOSE
    else:
        return Distance.TOO_FAR

def launchNavigationNode():#runs subscriber node of lidar that changes current state
    rospy.Subscriber("scan", LaserScan, executeNextMove)
    rospy.spin()


def updateState(data):
    #TODO make this update curr_state
    quadrant= [0,0,0]
    forward = []
    left = []
    right = []

    scan_size = len(data.ranges)
    increment = 0.0174930356443
    direction_spread = 0.4 #size of cone under consideration


    #organize data
    for i in range(scan_size):
        scan_val = data.ranges[i]
        scan_angle = i*increment

        #clean excessive values
        if(scan_val>data.range_max):
            scan_val = data.range_max
        if(scan_val<data.range_min):
            scan_val = data.range_min

        #sort into quadrant
        if(abs(scan_angle-3.14/2)<direction_spread):#ray is in forward cone
            #print(scan_val)
            forward.append(scan_val)
        elif(abs(scan_angle-3.14)<direction_spread):#ray is in left cone
            #print(scan_val)
            left.append(scan_val)
        elif(abs(scan_angle-0)<direction_spread or abs(scan_angle-6.28)<direction_spread):#ray is in right cone
            #print(scan_val)
            right.append(scan_val)
        else:
            continue


    #average data
    # quadrant[0] = np.mean(left)
    # quadrant[1] = np.mean(forward)
    # quadrant[2] = np.mean(right)

    #min data
    quadrant[0] = np.min(left)
    quadrant[1] = np.min(forward)
    quadrant[2] = np.min(right)


    #discretize data
    global curr_state
    curr_state = (getDistance(quadrant[0]),getDistance(quadrant[1]),getDistance(quadrant[2]))

    #print data
    #print(quadrant)
    debug_log() #prints out info about state and action



def debug_log():
    print(has_seen_wall)
    print(curr_state)
    print(curr_action)

# def move_q_table():
#     #TODO basic untrained movement
#     print("moving")
#     r = rospy.Rate(rosHZ)
#     while not rospy.is_shutdown():
#         updateActionBasic()#use current state and the manual q-table to update curr_action
#         #publish new cmd_vel based on most recent curr_action
#
#         #debug log
#         debug_log() #prints out info about state and action
#         r.sleep()
#
def executeNextMove(data):
    updateState(data)
    updateActionBasic()
    publishCurrentActionBasic()

def updateActionBasic():
    #manual q-table of 27 states with 3 actions left,right, or straight
    global has_seen_wall
    global curr_state
    global curr_action

    if(not has_seen_wall):#find a wall before starting q table algorithm
        #look for wall
        if(curr_state[0].value+curr_state[1].value+curr_state[2].value <9):#if they aren't all too FAR
            has_seen_wall = True
    else:
        if(qTableManual.has_key(curr_state)):
            q_values = qTableManual[curr_state]
            curr_action = q_values.index(max(q_values)) -1 #function converts the tuple of q-values to the action-code of the turn. i.e. if the max index is 0, meaning left-turn q val, the action code is -1. 1:0 and 2:1

def publishCurrentActionBasic():
    global pub
    directions = Pose2D()
    directions.theta = -curr_action*turnSpeed
    directions.y = linearSpeed
    pub.publish(directions)

if __name__ == '__main__':
    try:
        rospy.init_node('nav_q_table', anonymous=True)
        #global pub

        launchNavigationNode()

    except rospy.ROSInterruptException:
        pass
