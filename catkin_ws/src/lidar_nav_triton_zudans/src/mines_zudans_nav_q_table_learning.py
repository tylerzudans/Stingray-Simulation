#!/usr/bin/env python
import rospy
import random
import os
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Pose2D
import math
import time
import numpy as np
from enum import Enum



class Distance(Enum):
     TOO_CLOSE = 0
     CLOSE = 1
     MEDIUM = 2
     TOO_FAR = 3

#global state action vars
curr_state = (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.TOO_CLOSE)#left, forward, and right distances
curr_action = 0#curent turn action -1=turn left, 0 = none, 1 = right
prev_state = (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.TOO_CLOSE)#left, forward, and right distances
prev_action = 0#curent turn action -1=turn left, 0 = none, 1 = right
follow_right = True
has_seen_wall = False

#ros communication stuff
pub = rospy.Publisher('triton_lidar/vel_cmd', Pose2D, queue_size=2)
robot_radius = 0.15

#constants
rosHZ = 1
turnSpeed = 3
linearSpeed = 0.5
nextStepInfluence = 0.5

#q-vars
#noiseThreshold = 0.2 #if there are about equal values of 2 option, randomly choose
#randomChance = 0.2 #%chance to randomly pick next move
episode = 0
epsilon = 0.99 #discount Rate
learning_rate = 0.2
y=0.5#relative value of future reward
episode_value = 0
steps = 0
STEP_MAX = 1000

#q-table-manual: dictionary that takes in a 3-tuple of distances and outputs a tuple of action values
#used for right wall following
qTableManual =	{
#Follow Straight Wall at good distance
  (Distance.TOO_FAR,Distance.TOO_FAR,Distance.TOO_CLOSE): (1,0,0),
  (Distance.TOO_FAR,Distance.TOO_FAR,Distance.CLOSE): (0,1,0),
  (Distance.TOO_FAR,Distance.TOO_FAR,Distance.TOO_FAR): (0,0,1),
  (Distance.CLOSE,Distance.TOO_FAR,Distance.TOO_CLOSE): (1,0,0),
  (Distance.CLOSE,Distance.TOO_FAR,Distance.CLOSE): (0,1,0),
  (Distance.CLOSE,Distance.TOO_FAR,Distance.TOO_FAR): (0,0,1),
  (Distance.TOO_CLOSE,Distance.TOO_FAR,Distance.TOO_CLOSE): (1,0,0),
  (Distance.TOO_CLOSE,Distance.TOO_FAR,Distance.CLOSE): (0,1,0),
  (Distance.TOO_CLOSE,Distance.TOO_FAR,Distance.TOO_FAR): (0,0,1),

#left if theres an obstacle in from of you
  (Distance.TOO_FAR,Distance.CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.TOO_FAR,Distance.CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.TOO_FAR,Distance.CLOSE,Distance.TOO_CLOSE): (1,0,0),
  (Distance.TOO_FAR,Distance.TOO_CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.TOO_FAR,Distance.TOO_CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.TOO_FAR,Distance.TOO_CLOSE,Distance.TOO_CLOSE): (1,0,0),

  (Distance.CLOSE,Distance.CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.CLOSE,Distance.CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.CLOSE,Distance.CLOSE,Distance.TOO_CLOSE): (1,0,0),
  (Distance.CLOSE,Distance.TOO_CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.CLOSE,Distance.TOO_CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.CLOSE,Distance.TOO_CLOSE,Distance.TOO_CLOSE): (1,0,0),

  (Distance.TOO_CLOSE,Distance.CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.TOO_CLOSE,Distance.CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.TOO_CLOSE,Distance.CLOSE,Distance.TOO_CLOSE): (1,0,0),
  (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.TOO_FAR): (1,0,0),
  (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.CLOSE): (1,0,0),
  (Distance.TOO_CLOSE,Distance.TOO_CLOSE,Distance.TOO_CLOSE): (1,0,0),


}
def nextEpisode():
    global episode_value
    global episode
    global steps
    global has_seen_wall

    os.system("rosservice call /gazebo/reset_simulation")
    printQSum()
    print("Random Action Chance: "+ str(epsilon**episode))
    print("Episode "+str(episode)+ " finished in " + str(steps) + " steps")
    print("Episode Score: "+str(episode_value))
    episode = episode+1
    episode_value = 0
    steps = 0
    has_seen_wall = False

def printQSum():
    table_count = 0
    for key in qTableManual:
        for num in qTableManual[key]:
            table_count = table_count + num
    print("Debug Table Count: "+str(table_count))

def resetQTable(table):
    for key in table:
        table[key] = (0,0,0)
    return table

def r(the_state):#return r-value of states
    value = 0
    if(the_state[2]==Distance.CLOSE):#good to be close on the right
        value = value + 1
        if(the_state[1]==Distance.TOO_FAR):#better if front is far
            value = value + 0.2

    # if(the_state[1]==Distance.TOO_CLOSE):
    #     value = value - 0.5
    return value


def getDistance(distance):
    if(distance<0):
        return "Error negative distance"
    elif(distance<0.70):
        return Distance.TOO_CLOSE
    elif(distance<0.80):
        return Distance.CLOSE
    else:
        return Distance.TOO_FAR

def launchNavigationNode():#runs subscriber node of lidar that changes current state
    rospy.Subscriber("scan", LaserScan, executeNextMove)
    rospy.spin()


def updateState(data):
    #TODO make this update curr_state
    quadrant= [0,0,0]
    forward = []
    left = []
    right = []

    scan_size = len(data.ranges)
    increment = 0.0174930356443
    direction_spread = 0.4 #size of cone under consideration


    #organize data
    for i in range(scan_size):
        scan_val = data.ranges[i]
        scan_angle = i*increment

        #clean excessive values
        if(scan_val>data.range_max):
            scan_val = data.range_max
        if(scan_val<data.range_min):
            scan_val = data.range_min

        #sort into quadrant
        if(abs(scan_angle-3.14/2)<direction_spread):#ray is in forward cone
            #print(scan_val)
            forward.append(scan_val)
        elif(abs(scan_angle-3.14)<direction_spread):#ray is in left cone
            #print(scan_val)
            left.append(scan_val)
        elif(abs(scan_angle-0)<direction_spread or abs(scan_angle-6.28)<direction_spread):#ray is in right cone
            #print(scan_val)
            right.append(scan_val)
        else:
            continue


    #average data
    # quadrant[0] = np.mean(left)
    # quadrant[1] = np.mean(forward)
    # quadrant[2] = np.mean(right)

    #min data
    quadrant[0] = np.min(left)
    quadrant[1] = np.min(forward)
    quadrant[2] = np.min(right)


    #discretize data
    global curr_state
    global prev_state
    prev_state = curr_state
    curr_state = (getDistance(quadrant[0]),getDistance(quadrant[1]),getDistance(quadrant[2]))

    #print data
    #print(quadrant)
    #debug_log() #prints out info about state and action



def debug_log():
    print("State: "),
    print(curr_state)
    print('Action: '),
    print(curr_action)
    print('State Value: '),
    print(r(curr_state))

def executeNextMove(data):
    updateState(data)#TODO update previous stuff too
    updateScores()
    if(not endOfEpisode(data)):
        updateQTable()
        updateActionAdvanced()
        publishCurrentActionBasic()

def updateScores():
    global episode_value
    global steps
    episode_value = episode_value + r(curr_state)
    steps = steps+1

def endOfEpisode(data):
    global qTableManual
    global steps
    global STEP_MAX
    global has_seen_wall
    if(steps>STEP_MAX or (min(data.ranges)<max((data.range_min,robot_radius)) and steps>10)):#collision has occured well after start
        nextEpisode()
        f = open("dictionary.txt", "w")
        f.write(str(qTableManual))
        f.close()
        return True
    else:
        if (min(data.ranges)<max((data.range_min,robot_radius))):#collison has occured anytime
            has_seen_wall = False
        return False

def updateQTable():
    global curr_state
    global prev_state
    global prev_action
    global has_seen_wall
    global qTableManual
    prev_action_index = prev_action +1 #0,1,or 2
    if(curr_state!=prev_state and has_seen_wall):#state change has occured
        #print("STATE CHANGE")
        #update q
        values_at_state = list(qTableManual[prev_state])
        #print(values_at_state)
        #values_at_state[prev_action_index] = r(prev_state)+ nextStepInfluence*max(qTableManual[curr_state])
        values_at_state[prev_action_index] = qTableManual[prev_state][prev_action_index]+ learning_rate*(r(prev_state) + nextStepInfluence*max(qTableManual[curr_state]) - qTableManual[prev_state][prev_action_index])
        qTableManual[prev_state] = tuple(values_at_state)
        #print(qTableManual[prev_state])
        #print("")

        #qTableManual[prev_state][prev_action_index] = r(prev_state)+max(qTableManual[curr_state])#the last states estimated value is


def updateActionBasic():
    #manual q-table of 27 states with 3 actions left,right, or straight
    global has_seen_wall
    global curr_state
    global curr_action
    global prev_action
    global prev_state

    prev_state = curr_state
    prev_action = curr_action

    if(not has_seen_wall):#find a wall before starting q table algorithm
        #look for wall
        if(curr_state[0].value+curr_state[1].value+curr_state[2].value <9):#if they aren't all too FAR
            has_seen_wall = True
    else:
        if(qTableManual.has_key(curr_state)):
            q_values = qTableManual[curr_state]
            curr_action = q_values.index(max(q_values)) -1 #function converts the tuple of q-values to the action-code of the turn. i.e. if the max index is 0, meaning left-turn q val, the action code is -1. 1:0 and 2:1

def updateActionAdvanced():
    #manual q-table of 27 states with 3 actions left,right, or straight
    global has_seen_wall
    global curr_state
    global curr_action
    global prev_action

    prev_action = curr_action

    if(not has_seen_wall):#find a wall before starting q table algorithm
        #look for wall
        if(curr_state[0].value+curr_state[1].value+curr_state[2].value <9):#if they aren't all too FAR
            has_seen_wall = True
    else:#regular loop
        if(qTableManual.has_key(curr_state)):#set action to that with best value
            q_values = qTableManual[curr_state]
            curr_action = q_values.index(max(q_values)) -1 #function converts the tuple of q-values to the action-code of the turn. i.e. if the max index is 0, meaning left-turn q val, the action code is -1. 1:0 and 2:1

        if(random.uniform(0, 1)<epsilon**episode):#randomize next for low episodes
            curr_action = random.randint(-1,1)#pick number -[1,1]


def publishCurrentActionBasic():
    global pub
    global qTableManual
    directions = Pose2D()
    directions.theta = -curr_action*turnSpeed
    directions.y = linearSpeed
    pub.publish(directions)

if __name__ == '__main__':
    try:
        rospy.init_node('nav_q_table', anonymous=True)
        #global pub
        #rest qTable
        qTableManual = resetQTable(qTableManual)
        launchNavigationNode()

    except rospy.ROSInterruptException:
        pass
