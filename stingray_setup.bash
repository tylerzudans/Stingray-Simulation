#!/usr/bin/env bash

export GAZEBO_RESOURCE_PATH=~/TylerZudans-StingRaySim/catkin_ws/src/stingray_sim
export GAZEBO_MODEL_PATH=~/TylerZudans-StingRaySim/catkin_ws/src/stingray_sim/models
export GAZEBO_PLUGIN_PATH=~/TylerZudans-StingRaySim/catkin_ws/src/stingray_sim/plugins/build

